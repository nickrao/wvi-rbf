#*******************Importing required classes to build a testcase***************
from lib import url
from lib import Launch_Browser
from lib import Select_Hotel
from lib import Select_Room
from lib import Select_Flights
from lib import Guest_Info
from lib import Things_To_Do
from lib import Insurance_Page
from lib import Payment_Submission

#*****************Creating instance for the classes***********************

generate_url=url
hotel=Select_Hotel
roomsPage=Select_Room
flightsPage=Select_Flights
guestInfoPage = Guest_Info
thingsToDoPage = Things_To_Do
insurancePage = Insurance_Page
payment = Payment_Submission

#******************************TestSteps*************************************


hotel.Select_Hotel.non_hbsi()                         #Loading hotels page and electing hotel
roomsPage.Select_Room.room_select()                   #selecting first available room
flightsPage.Select_Flights.flights_select()           #proceeding with default flights
flightsPage.Select_Flights.plus_upgrade()             #selecting plus fare if requested
guestInfoPage.Guest_Info.guest_details()              #entering guest details
guestInfoPage.Guest_Info.contact_details()            #Entering contact details
guestInfoPage.Guest_Info.submit_guestinfoPage()       #submitting guest info page
thingsToDoPage.Things_To_DO.select_activities()       #selecting Things to do if requested and if available
insurancePage.Insurance_Page.sel_insurance()          #select insurance if requested else skip
payment.Payment_Submission.payment_summary()          #Accepting T&C's and submitting payment summary page

#*****************Executing based on selected FOP************************

payment_type = url.FOP
if payment_type == 'CC':
    payment.Payment_Submission.credit_card_payment()  # Creditcard Form of payment
elif payment_type == 'GC':
    payment.Payment_Submission.gift_card_payment()    # GiftCard Form of Payment





