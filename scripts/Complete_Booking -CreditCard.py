#------------
from lib import url
from lib import Launch_Browser
from lib import Select_Hotel
from lib import Select_Room
from lib import Select_Flights
from lib import Guest_Info
from lib import Things_To_Do
from lib import Insurance_Page
from lib import Payment_Submission

generate_url=url
hotel=Select_Hotel
roomsPage=Select_Room
flightsPage=Select_Flights
guestInfoPage = Guest_Info
thingsToDoPage = Things_To_Do
insurancePage = Insurance_Page
payment = Payment_Submission

#---------------------------------functions-----------------
hotel.Select_Hotel.non_hbsi()
roomsPage.Select_Room.room_select()
flightsPage.Select_Flights.flights_select()
flightsPage.Select_Flights.plus_upgrade()
guestInfoPage.Guest_Info.guest_details()
guestInfoPage.Guest_Info.contact_details()
guestInfoPage.Guest_Info.submit_guestinfoPage()
thingsToDoPage.Things_To_DO.select_activities()
insurancePage.Insurance_Page.sel_insurance()
payment.Payment_Submission.payment_summary()
payment.Payment_Submission.credit_card_payment()

