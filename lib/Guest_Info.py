# from selenium import webdriver
from faker import Faker
import random
from selenium.webdriver.support.select import Select
import datetime
import time

from lib import Launch_Browser
from lib import url
chrome=Launch_Browser
driver=Launch_Browser.driver
driver.implicitly_wait(10)
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
wait = WebDriverWait(driver, 10)

Adults=url.Adults
children=url.children
infants=0

class Guest_Info():

    def guest_details():
        fake = Faker()
        title = ['Mr', 'Mrs', 'Ms']
        first_name = fake.first_name()
        last_name = fake.last_name()

        num_of_adult = 1
        while num_of_adult<=Adults:
            #print("Number of adults:"+Adults)
            print("entering guest: %s details"%num_of_adult)
            dob = fake.date_between(start_date='-60y', end_date='-17y')
            title = ['Mr', 'Mrs', 'Ms']
            guestDetails={'Title':     random.choice(title),
                          'firstName':  first_name,
                          'lastName':   last_name,
                          'month':      dob.strftime('%m'),
                          'day':        dob.strftime('%d'),
                          'year':       dob.strftime('%Y')}
            num_of_adults=str(num_of_adult)

            title= (driver.find_element_by_css_selector("[name='TITLE"+num_of_adults+"']"))
            sel_title=Select(title)
            sel_title.select_by_value(guestDetails['Title'])

            fName=driver.find_element_by_css_selector("[name='FIRST_NAME"+num_of_adults+"']")
            fName.send_keys(guestDetails['firstName'])

            lName=driver.find_element_by_css_selector("[name='LAST_NAME"+num_of_adults+"']")
            lName.send_keys(guestDetails['lastName'])

            month_dob=driver.find_element_by_css_selector("[name='MONTH"+num_of_adults+"']")
            sel_month=Select(month_dob)
            sel_month.select_by_value(guestDetails['month'])

            day_dob=driver.find_element_by_css_selector("[name='DAY"+num_of_adults+"']")
            sel_day=Select(day_dob)
            sel_day.select_by_value(guestDetails['day'])

            year_dob=driver.find_element_by_css_selector("[name='YEAR"+num_of_adults+"']")
            sel_year=Select(year_dob)
            sel_year.select_by_value(guestDetails['year'])
            time.sleep(2)
            if num_of_adult< Adults:
                adult_num = num_of_adult + 3
                next_guest = str(adult_num)
                guest_drawer_css="[method] .drawer:nth-child(" + next_guest + ") [data-i18n='guest\.info\.adult']"
                guest_drawer_expand=wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,guest_drawer_css)))
                guest_drawer_expand.click()
            else:
                guest_drawer_expand.click()
            num_of_adult += 1

        # Login to enter Child details
        child_count=1
        while child_count <= children:
            print("entering child: %s details"%child_count)
            child_dob= fake.date_between(start_date='-12y', end_date='-2y')
            child_dob_day = dob.strftime('%d')
            child_dob_month = dob.strftime('%m')
            child_dob_year = dob.strftime('%Y')
            childDetails = {'Title': random.choice(title),
                            'firstName': first_name,
                            'lastName': last_name,
                            'month': child_dob_month,
                            'day': child_dob_day,
                            'year': child_dob_year}
            child_index = str(Adults + child_count)
            print("child number"+child_index)
            driver.find_element_by_css_selector("[name='TITLE" + child_index + "']").click()
            driver.find_element_by_css_selector("[name='TITLE" + child_index + "'] [value='" + childDetails['Title'] + "']").click()
            driver.find_element_by_css_selector("[name='FIRST_NAME" + child_index + "']").send_keys(childDetails['firstName'])
            driver.find_element_by_css_selector("[name='LAST_NAME" + child_index + "']").send_keys(childDetails['lastName'])
            driver.find_element_by_css_selector("[name='MONTH" + child_index + "']").click()
            driver.find_element_by_css_selector("[name='MONTH" + child_index + "'] [value='" + childDetails['month'] + "']").click()
            driver.find_element_by_css_selector("[name='DAY" + child_index + "']").click()
            driver.find_element_by_css_selector("[name='DAY" + child_index + "'] [value='" + childDetails['day'] + "']").click()
            driver.find_element_by_css_selector("[name='YEAR" + child_index + "']").click()
            driver.find_element_by_css_selector("[name='YEAR" + child_index + "'] [value='" + childDetails['year'] + "']").click()
            child_count+=1

        # logic to enter infant details
        infant_count=1
        while infant_count<=infants:
            print("entering infant: %s details"%infant_count)
            infant_dob =fake.date_between(start_date='-2y', end_date='now')
            infant_dob_day = dob.strftime('%d')
            infant_dob_month = dob.strftime('%m')
            infant_dob_year = dob.strftime('%Y')
            infantDetails = {'Title': random.choice(title),
                            'firstName': first_name,
                            'lastName': last_name,
                            'month': infant_dob_month,
                            'day': infant_dob_day,
                            'year': infant_dob_year}
            infant_index = str(Adults+children+infant_count)
            print("infant number" + infant_index)
            driver.find_element_by_css_selector("[name='TITLE" + infant_index + "']").click()
            driver.find_element_by_css_selector("[name='TITLE" + infant_index + "'] [value='" + childDetails['Title'] + "']").click()
            driver.find_element_by_css_selector("[name='FIRST_NAME" + infant_index + "']").send_keys(childDetails['firstName'])
            driver.find_element_by_css_selector("[name='LAST_NAME" + infant_index + "']").send_keys(childDetails['lastName'])
            driver.find_element_by_css_selector("[name='MONTH" + infant_index + "']").click()
            driver.find_element_by_css_selector("[name='MONTH" + infant_index + "'] [value='" + childDetails['month'] + "']").click()
            driver.find_element_by_css_selector("[name='DAY" + infant_index + "']").click()
            driver.find_element_by_css_selector("[name='DAY" + infant_index + "'] [value='" + childDetails['day'] + "']").click()
            driver.find_element_by_css_selector("[name='YEAR" + infant_index + "']").click()
            driver.find_element_by_css_selector("[name='YEAR" + infant_index + "'] [value='" + childDetails['year'] + "']").click()
            infant_count+=1
    def contact_details():
        fake = Faker()
        print("entering contact details")
        driver.find_element_by_id('contact-address').send_keys(fake.street_name())
        driver.find_element_by_id('contact-city').send_keys(fake.city())
        driver.find_element_by_id('contact-postalCode').send_keys('T3J0S2')
        driver.find_element_by_id('contact-phoneNumber').send_keys('403-555-5555')
        driver.find_element_by_id('contact-email').send_keys('nrao@westjet.com')
        driver.find_element_by_id('contact-confirmEmail').send_keys('nrao@westjet.com')
        driver.find_element_by_id('contact-altPhoneNumber').send_keys('403-555-5555')
        time.sleep(2)
    # Method to submit guestinfo page
    def submit_guestinfoPage():
        try:
         continue_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,".cta-extras")))
         continue_button.click()
         time.sleep(5)
         print("guest info page submitted")
        except NoSuchElementException:
            raise TimeoutException('continue button disabled')