from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from lib import Launch_Browser
driver=Launch_Browser.driver
wait = WebDriverWait(driver,10)

class Select_Room():
    def room_select():
        print("working to select room")
        rooms_available = driver.find_element_by_css_selector("#pagebody [action='verifforf\.cgi']")
        #num_of_roomtypes_available =rooms_available.__sizeof__()
        num_of_roomtypes_available = len(rooms_available.size)
        print("Available rooms: "+str(num_of_roomtypes_available-1))

        for rooms in range(num_of_roomtypes_available):
            try:
                room_css="#pagebody [action='verifforf\.cgi']:nth-of-type(1) [class='btn btn-primary btn-block select-room mb-2']"
                sel_room=wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,room_css)))
                sel_room.click()
                print('room selected')
                break
            except NoSuchElementException:
                print("No rooms availble")
        rooms+=1
