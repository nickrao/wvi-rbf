from selenium import webdriver

from lib import Launch_Browser
from lib import url


driver=Launch_Browser.driver
url=url

insurance=url.insurance

class Insurance_Page():
    def sel_insurance():
        insurance_exists = bool(insurance)
        if insurance_exists == True:
            insurance_package_css={'Deluxe Package':'.flex-row .card:nth-child(2) [name]',
                                  'Non-medical package':'.flex-row .card:nth-child(3) [name]',
                                  'Classic Medical':'.flex-row .card:nth-child(4) [name]'
                                  }

            if insurance == 'Deluxe Package':
                css = insurance_package_css['Deluxe Package']
            elif insurance == 'Non-medical package':
                css =insurance_package_css['Non-medical package']
            elif insurance =='Classic Medical':
                css=insurance_package_css['Classic Medical']

            select_button =driver.find_element_by_css_selector(""+css+"")
            select_button.click()
            print("insurance requested and selected: " +insurance)
        else:
            css = "[class='col-12 continue d-lg-flex justify-content-lg-end text-center'] [name]"
            select_button = driver.find_element_by_css_selector("" + css + "")
            select_button.click()
            print("insurance not requested, continuing without insurance")