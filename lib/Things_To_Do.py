from selenium import webdriver
import time
from lib import Launch_Browser
from lib import url
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By

url=url
chrome=Launch_Browser
driver=Launch_Browser.driver
things_to_do = url.things_to_do
wait = WebDriverWait(driver, 20)
class Things_To_DO():
    def select_activities():
        time.sleep(5)
        thingsToDoPageUrl = 'https://staging-wes.prebook.softvoyage.com/cgi-bin/verifforf2.cgi'
        current_url = driver.current_url
        print("page loaded after guestinfo: " +current_url)
        if thingsToDoPageUrl == current_url:
            print("Working to select an activity, if requested")
            activities= things_to_do.upper()
            if activities=='YES':
                first_available_activity = driver.find_element_by_css_selector("[class] .attraction:nth-of-type(1) .attraction-name")
                first_available_activity.click()
                time.sleep(2)
                first_checkbox = driver.find_element_by_css_selector("[for='attraction-1-1']")
                first_checkbox.click()
                time.sleep(15)
                continue_button= wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,".continue .btn-primary")))
                #continue_button=driver.find_element_by_css_selector(".continue .btn-primary")
                continue_button.click()
                print("Activity requested and selected")
            else:
                 continue_button = driver.find_element_by_css_selector(".continue .btn-primary")
                 continue_button.click()
                 print("activity not requested, Continuing without activity")
