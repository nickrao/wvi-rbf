from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from lib import url

print("launching chrome browser")
driver = webdriver.Chrome()

class Launch_Browser():
    def chrome_browser():
        #driver.maximize_window()
        driver.set_window_size(1400, 1000)
        driver.get(url.deeplink_url)
        driver.implicitly_wait(3)
        print("chrome browser launched")
