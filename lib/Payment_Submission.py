from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from lib import Launch_Browser
driver=Launch_Browser.driver
wait = WebDriverWait(driver, 10)


class Payment_Submission:
    def payment_summary():
        accept_tc = driver.find_element_by_css_selector("label")
        accept_tc.click()
        time.sleep(2)
        continue_button= wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,".float-right .btn-payment")))
        continue_button.click()

    def credit_card_payment():
        print("Entering creditcard details")
        cc_num = driver.find_element_by_id("payment-info-number")
        cc_num.send_keys("4030000010001234")
        expirydate_month = Select(driver.find_element_by_id('payment-card-expiry-month'))
        expirydate_month.select_by_value('02')
        expirydate_year = Select(driver.find_element_by_id('payment-card-expiry-year'))
        expirydate_year.select_by_value('2020')
        cc_cvv = driver.find_element_by_id('payment-info-CVV')
        cc_cvv.send_keys('123')
        cc_holdername = driver.find_element_by_id('payment-info-cardholder')
        cc_holdername.send_keys('westjet')
        time.sleep(2)
        submit_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,"[method] button")))
        submit_button.click()
        time.sleep(10)
        driver.close()

    def gift_card_payment():
        print("entering giftcard details")
        gc_drawer_expand = driver.find_element_by_css_selector("[name='frmGC'] .toggle-header [role]")
        gc_drawer_expand.click()
        gc_num = driver.find_element_by_id('gc-number-input')
        gc_num.send_keys('144403199917314')
        gc_pin =driver.find_element_by_id("gc-access-code-input")
        gc_pin.send_keys("4366")
        check_gc_balance = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "gc-modal-checkBalance")))
        check_gc_balance.click()
        time.sleep(10)
        driver.close()