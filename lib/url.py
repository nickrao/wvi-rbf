import urllib.parse
import csv
from faker import Faker



#******************************TEST DATA***************************
file_path ='/Users/nikethanraopamera/PycharmProjects/seleniumProject/selenium_datafiles/wvi_datafile.csv'

with open(file_path, 'r') as infile:
  # read the file as a dictionary for each row ({header : value})
  reader = csv.DictReader(infile)
  data = {}
  for row in reader:
    for header, value in row.items():
      try:
        data[header].append(value)
      except KeyError:
        data[header] = [value]

row_count = data['origin']
Num_of_rows=len(row_count)

for row_num in range(Num_of_rows):
    alternate_flights = data['alternateFlights'][row_num]
    plus = data['selectPlus'][row_num]
    things_to_do = data['thingsToDo'][row_num]
    insurance = data['insurance'][row_num] #'Deluxe Package'

    #****************Payment Details****************************
    form_of_payment = data['FOP'][row_num]
    card_Num= data['CC_Num'][1][1:]
    card_cvv = data['cc_cvv']
    fake = Faker()
    expiry =fake.date_between(start_date='now', end_date='+5y')
    expiry_month = expiry.strftime('%m')
    expiry_year = expiry.strftime('%Y')

    class url():
        def __init__(self, staging_url):
            #url = 'https://vacationbookings.westjet.com/cgi-bin/resultspackage-plus.cgi?'
            url = 'https://staging-wes.presax.softvoyage.com/cgi-bin/resultspackage-plus.cgi?'

            getvars = {'alias': 'wes',
                       'language': 'en',
                       'gateway_dep': data['origin'][row_num],
                       'dest_dep': data['destination'][row_num],
                       'date_dep': str(data['dep_date'][row_num]),
                       'date_ret': str(data['return_date'][row_num]),
                       'occupancy': data['occupancy'][row_num],
                       'nb_adult': data['adults'][row_num],
                       'nb_child': data['child'][row_num], 'child_1_age': '', 'child_2_age': '', 'child_3_age': '', 'child_4_age': '',
                       'child_5_age': '', 'child_6_age': '',
                       'uc': '0',
                       'search_btn': 'search for vacations'}

            staging_url = url + urllib.parse.urlencode(getvars)
            self.staging_url = staging_url

    parameters = url('')
    deeplink_url = parameters.staging_url
    print(deeplink_url)
    Adults = int(data['adults'][row_num])
    children = int(data['child'][row_num])
    FOP = str(data['FOP'][row_num]).upper()
    row_num+=1



