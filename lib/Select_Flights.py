from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from lib import Launch_Browser
from lib import url
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
url=url
driver=Launch_Browser.driver
wait = WebDriverWait(driver,10)

#Extracting Testdata from url.py
alternate_flights=url.alternate_flights
plus=url.plus

#creating class to handle flight selection methods
class Select_Flights():
    def flights_select():
        alt_flights=alternate_flights.upper()
        if alt_flights=='YES':
            driver.find_element_by_css_selector(".text-right [type='button']:nth-of-type(1)").click()
        else:
             driver.find_element_by_css_selector(".text-right [type='button']:nth-of-type(2)").click()
             print("Continuing with default selected flights")

    def plus_upgrade():
        pluspage_url = 'https://staging-wes.presax.softvoyage.com/cgi-bin/upgradetoplus.cgi'
        currentpage_url = driver.current_url
        print("page loaded after flights page:"+currentpage_url)
        if pluspage_url == currentpage_url:
            plus_upgrade=plus.upper()
            if plus_upgrade == 'YES':
                plus_css = ".text-right [type='button']:nth-of-type(2)"
                selectPlus= wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,plus_css)))
                selectPlus.click()
                print("Plus fare requested and selected")
            else:
                skipPlus_css = ".text-right [type='button']:nth-of-type(1)"
                skipPlus = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,skipPlus_css)))
                skipPlus.click()
                print("plus fare not requested, continuing without plus fare")