from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from lib import Launch_Browser

driver = Launch_Browser.driver
chrome = Launch_Browser
chrome.Launch_Browser.chrome_browser()

class Select_Hotel():
    def non_hbsi():
        print("working to select NON-HBSI hotel")
        cnt_nonhbsi_hotels = len(driver.find_elements_by_css_selector('div.result-wrapper:not([rel$="2"])'))
        for num_hbsi in range(0,cnt_nonhbsi_hotels):
            css_nthType=str(num_hbsi+1)
            try:
                css = "#soft-divresults .hotel.card:not([rel$='2']):nth-of-type(" + css_nthType + ") [class='col-4 d-none d-md-block'] .select-room"
                hotel=driver.find_element_by_css_selector(css)

                if hotel.is_displayed():
                    hotel.click()
                    break
            except NoSuchElementException:
                print('hotels iteration', +num_hbsi)
        num_hbsi+=1
        print("Non-HBSI selected Hotel")

